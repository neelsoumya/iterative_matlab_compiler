#!/bin/bash

##################################################################################
# Function - iterative_matlab_compiler.sh
# Shell script to iterate through directories in pwd
# and compile matlab code in each of those directories
#
# Parameters - Pass the filename (full filename of m file) as an argument 
#
# Example - ./iterative_matlab_compiler.sh test_script.m
# where the name of the matlab file is test_script.m
#
# Notes - 1) Pass the full filename as an argument 
#	  2) Assumes that MATLAB is installed	
#	  3) Assumes the name of the file is the same in all directories  	
# 
# Author - Soumya Banerjee
# Website - https://sites.google.com/site/neelsoumya/
#	    www.cs.unm.edu/~soumya	
# Creation Date - 24th Dec 2014
# License - GNU GPL
###################################################################################

# Check arguments
if [ $# -eq 0 ] 
then
    echo "$0:Error command arguments missing!"
    echo "Usage: $0  filename"
    exit 1
fi


# Check if file extension correct
file_extension=`echo $1 | awk -F . '{print $NF}' `

if [ $file_extension = 'm' ]
then
	echo 'File extension correct'
else
	echo "$0: Error, incorrect file extension (expects .m file)"
	exit 1
fi



# Iterate through directories and compile code
for dir in */
do
        echo $dir

        # go to directory
        cd "$dir"

        # run mcc to compile matlab code
	# check if the file exists
	if [ -f $1 ]
	then
	        /usr/local/MATLAB/R2011a/bin/mcc -mv $1
        	# Alternative: /opt/matlab/bin/mcc -mv $1
	else
        	echo "file not found in directory: "
		pwd
	fi
	
        # go back to parent directory
        cd ..
done

